Los cuatro mayores cambios fueron:

- Agregar una ruta que respondiera al método `DELETE` desde "/carrito":

```ruby
delete "/carrito", to: "in_shopping_carts#destroy"
```

- Actualizar el método `destroy` en `InShoppingCartsController` para que respondiera con javascript:

```ruby
def destroy
  @in_shopping_cart = InShoppingCart.find(params[:id])
  @in_shopping_cart.destroy
  respond_to :js
end
```

- Editar la línea en la cual se le asignaba un id a cada `tr`:

```ruby
-product = i_sh.product
  %tr{id: "i_sh-#{i_sh.id}"}
    %td= product.name
    %td= product.pricing
```

- Actualizar el archivo `destroy.js.erb`, no era necesario bindear la acción del botón:

```ruby
$('tr#i_sh-<%= @in_shopping_cart.id %>').fadeOut(500, function(){
  $(this).hide();
});
```